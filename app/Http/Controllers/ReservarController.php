<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;
use App\Reservar;
use Illuminate\Support\Facades\Log;


class ReservarController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";

    public function arregloColumnasReservar()
    {
        return array(
            0 => 'reservas.nombre',
            1 => 'restaurante.nombre'
        );
    }

     public function contarReservar($consulta)
    {
        return $consulta->count();
    }

    public function obtenerReservar($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaReservar($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {

            //dd($inicio.$limite);
            //die();
            $consulta->offset($inicio)
                ->limit($limite);

            //dd($consulta->tosql());
            //die();
        }

        return $consulta;
    }

    private function ordenarConsultaReservar($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaReservar($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function Reservar(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonaReservar();

        $nombrepersonaaccesa= self::obtenernombrepersonaReservar();

        return view('admin.reservar.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function ReservarEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();

        $consulta = DB::table('reservas')
                ->join('restaurante','restaurante.id', '=', 'reservas.idrest')
                ->select(
                    'reservas.id',
                    'reservas.nombre',
                    'restaurante.nombre as restaurante' ,
                     DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha')
                )
                ->distinct()
                ->whereNull('reservas.deleted_at')
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function ReservarGeneral()
    {


        //$consulta = DB::table('restaurante')
        return DB::table('reservas')
                ->join('restaurante','restaurante.id', '=', 'reservas.idrest')
                ->select(
                    'reservas.id',
                    'reservas.nombre',
                    'restaurante.nombre as restaurante' ,
                     DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha')
                )
                ->distinct()
                ->whereNull('reservas.deleted_at')
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');

        //dd($consulta);
        //die();

    }

     private function BuscarEnConsultaFiltroReservar($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    public function ListarReservar(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasReservar();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_reservar  = $request->input('dp_fedesde_reservar');
            $dp_fehasta_reservar  = $request->input('dp_fehasta_reservar');

            $fechadesde = explode("-",$dp_fedesde_reservar);
            $fechahasta = explode("-",$dp_fehasta_reservar);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


            $consulta = self::ReservarEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::ReservarGeneral();
          
        }

        $consulta  = self::buscarEnConsultaReservar($consulta, $buscar, $columns);

        $total = self::contarReservar($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaReservar($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listareservar = self::obtenerReservar($consulta);

        //dd($listaadmirestaurant);
        //die();


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listareservar,
            //"totaltardanza" => $totaltardanza,
            //"totalantessalida" => $totalantessalida,

        );

     

        return response()->json($json_data);
    
    }

    public function CargarRestaurante(Request $request)
    {

        $restaurante = DB::table('restaurante')
            ->select('id','nombre')
            ->distinct()
            ->orderBy('id','asc')
            ->get();

        //dd($almacen);
        //die();

        return response()->json($restaurante);

    }


    public function BuscarCantidadReservas(Request $request,$idrestaurant,$fecha)
    {

        $fecha = explode("-",$fecha);
           
        $fechaformateada =  $fecha[0].$fecha[1].$fecha[2];

        $reservas = DB::table('reservas')
            ->select(
              
              'id'
    
            )
           
            ->where('reservas.idrest',$idrestaurant)
            ->where('reservas.fecha',$fechaformateada)
            ->get();

        $contador = (int)$reservas->count();

        //dd($contador);
        //die();


        if($contador==15){

            $cantidadreservas="CANTIDAD DE RESERVAS SUPERADA";

        }
        else{

            $cantidadreservas="CANTIDAD DE RESERVAS NO SUPERADA";

        }


        $respuesta = array(
                    "cantidadreservas"  => $cantidadreservas,
        );

        return response()->json($respuesta);

    }


    public function RecargarRestaurantes(Request $request, $criterio)
    {

        $criterio = "%" . $criterio . "%";

        $restaurante = DB::table('restaurante')
            ->select('id','nombre')
            ->distinct()
            ->where('nombre', 'like', $criterio)
            ->orderBy('id','asc')
            ->get();
            
        return response()->json($restaurante);

    }
    

    public function IngresarReserva(Request $request)
    {

        if ($request->ajax()) {

            $nombrereserva = trim($request['nombrereserva']);

            $fecha = $request['fecha'];

            $fecha = explode("-",$fecha);
           
            $fechaformateada =  $fecha[0].$fecha[1].$fecha[2];

            $idrestaurante = $request['restaurante'];
           

            DB::beginTransaction();

            try {
                 
                $utimoid = DB::select(DB::raw('select id from reservas where id = (select max(`id`) from reservas )'));
                
                if($utimoid==null){
                   $idingresar= 1;
                }
                else{

                    $ultimo = trim($utimoid[0]->id);
                    $idingresar= $ultimo + 1;
                    $idingresar= (int)$idingresar;
                }

                //dd($idingresar."  ".$fechasoli."  ".$horasoli."  ".$codpersona."  ".$supervisor);
                //die();

                $restaurant = Reservar::create([
                "id"  => $idingresar,
                "nombre"    => $nombrereserva,
                "idrest"  => $idrestaurante,
                "fecha"    => $fechaformateada,
                ]);


            DB::commit();

                $respuesta = array(
                    "mensaje"  => "creado",
                );

                

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "creado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }
    
     public function obtenerfotopersonaReservar()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('users')
                ->select(
                     'users.fimagen'  
                )
                ->distinct()
                ->where('users.co_usuario',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

    
        return($foto);
    }

    public function obtenernombrepersonaReservar()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('users')
                ->select(
                    'users.nombreusuario AS personaaccesa'
                )
                ->distinct()
               ->where('users.co_usuario',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }




}
