<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;
use App\Restaurante;
use Illuminate\Support\Facades\Log;


class AdministrarRestaurantController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";

    public function arregloColumnasAdmirestaurant()
    {
        return array(
            0 => 'nombre',
            1 => 'ciudad'
        );
    }

     public function contarAdmirestaurant($consulta)
    {
        return $consulta->count();
    }

    public function obtenerAdmirestaurant($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaAdmirestaurant($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {

            //dd($inicio.$limite);
            //die();
            $consulta->offset($inicio)
                ->limit($limite);

            //dd($consulta->tosql());
            //die();
        }

        return $consulta;
    }

    private function ordenarConsultaAdmirestaurant($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaAdmirestaurant($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function Admirestaurant(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonaAdmirestaurant();

        $nombrepersonaaccesa= self::obtenernombrepersonaAdmirestaurant();

        return view('admin.administrarrestaurant.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function AdmirestaurantEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();

        $consulta = DB::table('restaurante')
                ->select(
                    'id',
                    'nombre',
                    'descripcion',
                    'direccion',
                    'ciudad',
                    'urlfoto'
                )
                ->distinct()
                ->whereNull('restaurante.deleted_at')
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id', 'asc');
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function AdmirestaurantGeneral()
    {


        //$consulta = DB::table('restaurante')
        return DB::table('restaurante')
                ->select(
                    'id',
                    'nombre',
                    'descripcion',
                    'direccion',
                    'ciudad',
                    'urlfoto'
                )
                ->distinct()
                ->whereNull('restaurante.deleted_at')
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');

        //dd($consulta);
        //die();

    }

     private function BuscarEnConsultaFiltroAdmirestaurant($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    public function ListarAdmirestaurant(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasAdmirestaurant();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_admirestaurant  = $request->input('dp_fedesde_admirestaurant');
            $dp_fehasta_admirestaurant  = $request->input('dp_fehasta_admirestaurant');

            $fechadesde = explode("-",$dp_fedesde_admirestaurant);
            $fechahasta = explode("-",$dp_fehasta_admirestaurant);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


            $consulta = self::AdmirestaurantEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::AdmirestaurantGeneral();
          
        }

        $consulta  = self::buscarEnConsultaAdmirestaurant($consulta, $buscar, $columns);

        $total = self::contarAdmirestaurant($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaAdmirestaurant($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listaadmirestaurant = self::obtenerAdmirestaurant($consulta);

        //dd($listaadmirestaurant);
        //die();


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listaadmirestaurant,
            //"totaltardanza" => $totaltardanza,
            //"totalantessalida" => $totalantessalida,

        );

     

        return response()->json($json_data);
    
    }

    public function IngresarAdmirestaurant(Request $request)
    {

        if ($request->ajax()) {

            $nombre = trim($request['nombre']);
            $descripcion = trim($request['descripcion']);
            $ciudad = trim($request['ciudad']);
            $direccion = trim($request['direccion']);

            $contenidofoto = file_get_contents($request['foto']);

            DB::beginTransaction();

            try {
                 
                $utimoid = DB::select(DB::raw('select id from restaurante where id = (select max(`id`) from restaurante )'));
                
                if($utimoid==null){
                   $idingresar= 1;
                }
                else{

                    $ultimo = trim($utimoid[0]->id);
                    $idingresar= $ultimo + 1;
                    $idingresar= (int)$idingresar;
                }

                //dd($idingresar."  ".$fechasoli."  ".$horasoli."  ".$codpersona."  ".$supervisor);
                //die();

                $restaurant = Restaurante::create([
                "id"  => $idingresar,
                "nombre"    => $nombre,
                "descripcion"  => $descripcion,
                "direccion"    => $direccion,
                "ciudad"    => $ciudad,
                "foto"    => $contenidofoto,
                ]);


            DB::commit();

                $respuesta = array(
                    "mensaje"  => "creado",
                );

                

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "creado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }


    public function ObtenerRestaurant(Request $request, $id)
    {

            $restaurant = DB::table('restaurante')
                ->select(
                        'id',
                        'nombre',
                        'descripcion',
                        'direccion',
                        'ciudad',
                        'foto'
                        )
                ->where('id', $id)
                        ->whereNull('restaurante.deleted_at')
                        ->get();
            
            $foto = trim($restaurant[0]->foto);

            $contenidoImagen = base64_encode($foto); 


            $respuesta = array(
                    "id"  => $restaurant[0]->id,
                    "nombre" => $restaurant[0]->nombre,
                    "descripcion" => $restaurant[0]->descripcion,
                    "direccion" => $restaurant[0]->direccion,
                    "ciudad" => $restaurant[0]->ciudad,
                    "foto" => $contenidoImagen,
            );

            //mb_convert_encoding($respuesta, 'UTF-8', 'UTF-8'); 

            //dd($respuesta);
            //die();

        return response()->json($respuesta);  
    }

    public function ActualizarRestaurant(Request $request)
    {

        if ($request->ajax()) {

            $id =  $request['id'];

            $nombre = trim($request['nombre']);

            $descripcion = trim($request['descripcion']);

            $direccion = trim($request['direccion']);

            $ciudad = trim($request['ciudad']);

            $contenidoImagen = file_get_contents($request['imagen']);

            //dd($codigoconcepto);
            //die();

            DB::beginTransaction();

            try {
                 
                $restaurant = Restaurante::where('id',$id)
                        ->update([
                            "nombre" => $nombre,
                            "descripcion" => $descripcion,
                            "direccion" => $direccion,
                            "ciudad" => $ciudad,
                            "foto"=>$contenidoImagen,
                ]);

                DB::commit();

                $respuesta = array(
                    "mensaje"  => "actualizado",
                );

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "actualizado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }


     public function AsosiacionRestaurant(Request $request, $id)
    {

        if($request->ajax()){

            DB::beginTransaction();

            try {

                $encontrado="NO";

                $buscar = DB::table('reservas')
                ->select(
              
                    'idrest'
    
                )
                ->where('reservas.idrest',$id)
                ->get();

                $contador = (int)$buscar->count();

                if($contador>0){
                    $encontrado="SI";

                }

                $respuesta = array(
                         "restauranteencontrado" => $encontrado,
                );

                return response()->json($respuesta); 
  

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                throw $e;
            }


        }
    }

    
    public function EliminarRestaurant(Request $request,$id)
    {

        if ($request->ajax()) {

            $date = trim(Carbon::now());

            DB::beginTransaction();

            try {
                 
                $restaurant = Restaurante::where('id',$id)
                        ->update([
                             'deleted_at'=>$date
                ]);

                DB::commit();

                $respuesta = array(
                    "mensaje"  => "eliminado",
                );

                return response()->json($respuesta);

                //return response()->json(["mensaje" => "actualizado"]);

            } catch (\Throwable $e) {
                DB::rollback();
                Log::error(sprintf("%s - linea %d - Error %s", __METHOD__, __LINE__, $e->getMessage()));
                return response()->json(["error" => $e->getMessage()]);
            }

        }

    }
   

    
     public function obtenerfotopersonaAdmirestaurant()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('users')
                ->select(
                     'users.fimagen'  
                )
                ->distinct()
                ->where('users.co_usuario',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

    
        return($foto);
    }

    public function obtenernombrepersonaAdmirestaurant()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('users')
                ->select(
                    'users.nombreusuario AS personaaccesa'
                )
                ->distinct()
               ->where('users.co_usuario',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }




}
