<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Log;


class ListarRestaurantController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";

    public function arregloColumnasListarrestaurant()
    {
        return array(
            0 => 'nombre',
            1 => 'ciudad'
        );
    }

     public function contarListarrestaurant($consulta)
    {
        return $consulta->count();
    }

    public function obtenerListarrestaurant($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaListarrestaurant($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {

            //dd($inicio.$limite);
            //die();
            $consulta->offset($inicio)
                ->limit($limite);

            //dd($consulta->tosql());
            //die();
        }

        return $consulta;
    }

    private function ordenarConsultaListarrestaurant($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaListarrestaurant($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function Restauranteslistar(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonaListarrestaurant();

        $nombrepersonaaccesa= self::obtenernombrepersonaListarrestaurant();

        return view('admin.listarrestaurant.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function ListarrestaurantEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        //dd($buscar,$fe_desde,$fe_hasta);
        //die();

        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        //dd($fe_desde.$fe_hasta);
        //die();

        $consulta = DB::table('restaurante')
                ->select(
                    'id',
                    'nombre',
                    'descripcion',
                    'direccion',
                    'ciudad',
                    'urlfoto'
                )
                ->distinct()
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id', 'asc');
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function ListarrestaurantGeneral()
    {


        //$consulta = DB::table('restaurante')
        return DB::table('restaurante')
                ->select(
                    'id',
                    'nombre',
                    'descripcion',
                    'direccion',
                    'ciudad',
                    'urlfoto'
                )
                ->distinct()
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');

        //dd($consulta);
        //die();

    }
   
    public function Listarrestaurant(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasListarrestaurant();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');


        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            //dd("pase por aqui");
            //die();
            $dp_fedesde_admirestaurant  = $request->input('dp_fedesde_listarrestaurant');
            $dp_fehasta_admirestaurant  = $request->input('dp_fehasta_listarrestaurant');

            $fechadesde = explode("-",$dp_fedesde_listarrestaurant);
            $fechahasta = explode("-",$dp_fehasta_listarrestaurant);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];


            $consulta = self::ListarrestaurantEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::ListarrestaurantGeneral();
          
        }

        $consulta  = self::buscarEnConsultaListarrestaurant($consulta, $buscar, $columns);

        $total = self::contarListarrestaurant($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaListarrestaurant($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listarrestaurant = self::obtenerListarrestaurant($consulta);

        //dd($listaadmirestaurant);
        //die();


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listarrestaurant,
            //"totaltardanza" => $totaltardanza,
            //"totalantessalida" => $totalantessalida,

        );

     

        return response()->json($json_data);
    
    }

   
    
     public function obtenerfotopersonaListarrestaurant()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('users')
                ->select(
                     'users.fimagen'  
                )
                ->distinct()
                ->where('users.co_usuario',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

    
        return($foto);
    }

    public function obtenernombrepersonaListarrestaurant()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('users')
                ->select(
                    'users.nombreusuario AS personaaccesa'
                )
                ->distinct()
               ->where('users.co_usuario',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }




}
