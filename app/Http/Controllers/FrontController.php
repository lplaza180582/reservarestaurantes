<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;


class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::check())
        {

            $usuario_actual = \Auth::User();

            $co_usuario = $usuario_actual->co_usuario;

            $foto= self::obtenerfotopersona();

            $nombrepersonaaccesa= self::obtenernombrepersona();

            return view('admin.index')
                 ->with("usuario_actual", $usuario_actual)
                 ->with("fotopersona",$foto)
                 ->with("personaaccesa",$nombrepersonaaccesa);
        }
        else
        {

         return view('admin.login.index');
            
        }
    }

    public function admin()
    {
        
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $foto= self::obtenerfotopersona();

        $nombrepersonaaccesa= self::obtenernombrepersona();
            
        return view('admin.index')
                ->with("usuario_actual", $usuario_actual)
                ->with("fotopersona",$foto)
                ->with("personaaccesa",$nombrepersonaaccesa);
    }

    public function obtenerfotopersona()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('users')
                ->select(
                     'users.fimagen'  
                )
                ->distinct()
                ->where('users.co_usuario',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

    
        return($foto);
    }

    public function obtenernombrepersona()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('users')
                ->select(
                    'users.nombreusuario AS personaaccesa'
                )
                ->distinct()
               ->where('users.co_usuario',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

   

}
