<?php

namespace App\Http\Controllers;

//use App\Http\Requests\DistribuirCreateRequest;
//use App\Http\Requests\CargarUpdateRequest;

//use App\HistorialCarga;

//use App\Proveedor;

use Auth;
use Carbon\Carbon;
use DB;
use Excel;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Log;


class ListarReservarController extends Controller
{
    private $tipoBusqueda = "especifico";

    //private $tipoBusqueda = "especifico";

    public function arregloColumnasListarReservar()
    {
        return array(
            0 => 'reservas.nombre',
            1 => 'restaurante.nombre'
        );
    }

     public function contarListarReservar($consulta)
    {
        return $consulta->count();
    }

    public function obtenerListarReservar($consulta)
    {
        return $consulta->get();
    }

    private function limitarConsultaListarReservar($consulta, $inicio, $limite)
    {
        if ($limite != null && $inicio != null) {

            //dd($inicio.$limite);
            //die();
            $consulta->offset($inicio)
                ->limit($limite);

            //dd($consulta->tosql());
            //die();
        }

        return $consulta;
    }

    private function ordenarConsultaListarReservar($consulta, $orden, $dir)
    {
        if ($orden != null && $dir != null) {
            $consulta->orderBy($orden, $dir);
        }
        return $consulta;
    }

    private function buscarEnConsultaListarReservar($consulta, $buscar, $columnas)
    {
        if (!empty($buscar)) {
            $consulta->where(function($query) use($columnas, $buscar) {
                foreach($columnas as $key => $column) {
                    $query->orWhere($column, 'like', "%{$buscar}%");
                }
            });
        }
        return $consulta;
    }


    public function ReservasListar(Request $request)
    {
        $fecha = Carbon::now();
        $fecha = $fecha->format('Y-m-d');

        $foto= self::obtenerfotopersonaListarReservar();

        $nombrepersonaaccesa= self::obtenernombrepersonaListarReservar();

        return view('admin.listarreservar.index')
            ->with("fecha", $fecha)
            ->with("fotopersona",$foto)
            ->with("personaaccesa",$nombrepersonaaccesa);
    }

    

    private function ListarReservarEspecifica($buscar,$fe_desde,$fe_hasta)
    {

        $consulta = DB::table('reservas')
                ->join('restaurante','restaurante.id', '=', 'reservas.idrest')
                ->select(
                    'reservas.id',
                    'reservas.nombre',
                    'restaurante.nombre as restaurante' ,
                     DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha')
                )
                ->distinct()
                ->whereNull('reservas.deleted_at')
                ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');
       
        //dd($consulta->tosql());
        //dd($consulta);
        //die();
        return $consulta;

    }

    private function ListarReservarGeneral()
    {


        //$consulta = DB::table('restaurante')
        return DB::table('reservas')
                ->join('restaurante','restaurante.id', '=', 'reservas.idrest')
                ->select(
                    'reservas.id',
                    'reservas.nombre',
                    'restaurante.nombre as restaurante' ,
                     DB::raw('DATE_FORMAT(fecha, "%d/%m/%Y") as fecha')
                )
                ->distinct()
                ->whereNull('reservas.deleted_at')
               // ->whereBetween('fecha',[$fe_desde,$fe_hasta])
                ->orderBy('id','asc');

        //dd($consulta);
        //die();

    }

     private function BuscarEnConsultaFiltroListarReservar($consulta, $buscar)
    {

        if(!empty($buscar)){

            $consulta->Where('dia', 'like', "{$buscar}%")
             ->orWhere('cfecha', 'like', "{$buscar}%")
                ->orWhere('entrada', 'like', "{$buscar}%");
              
               
        }
        return $consulta;
    }

   
    public function Listarreservas(Request $request)
    {

        $listatotal = [];
        $columns = self::arregloColumnasListarReservar();
        $limite = $request->input('length');
        $inicio = $request->input('start');
        $orden = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar = $request->input('search.value');

        $total = 0;
        $tipoBusqueda = $request->input('tipo');

        //dd("pase por aqui");
        //die();


        if (strcasecmp($tipoBusqueda, $this->tipoBusqueda) == 0) {

            
            $dp_fedesde_listarreservar  = $request->input('dp_fedesde_listarreservar');
            $dp_fehasta_listarreservar  = $request->input('dp_fehasta_listarreservar');

            $fechadesde = explode("-",$dp_fedesde_listarreservar);
            $fechahasta = explode("-",$dp_fehasta_listarreservar);

            $fechadesdeformateada =  $fechadesde[0].$fechadesde[1].$fechadesde[2];
            $fechahastaformateada =  $fechahasta[0].$fechahasta[1].$fechahasta[2];

            //dd("pase por aqui".$fechadesdeformateada);
            //die();


            $consulta = self::ListarReservarEspecifica($buscar,$fechadesdeformateada,$fechahastaformateada);

        }else{


            $consulta = self::ListarReservarGeneral();
          
        }

        $consulta  = self::buscarEnConsultaListarReservar($consulta, $buscar, $columns);

        $total = self::contarListarReservar($consulta);
        //dd($total);
        //die();
        $consulta = self::limitarConsultaListarReservar($consulta, $inicio, $limite);
        //$consulta = self::ordenarConsulta($consulta, $orden, $dir);
        $listareservar = self::obtenerListarReservar($consulta);

        //dd($listaadmirestaurant);
        //die();


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => $total,
            "recordsFiltered" => $total,
            "data" => $listareservar,
            //"totaltardanza" => $totaltardanza,
            //"totalantessalida" => $totalantessalida,

        );

     

        return response()->json($json_data);
    
    }


     public function obtenerfotopersonaListarReservar()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $fotopersona = DB::table('users')
                ->select(
                     'users.fimagen'  
                )
                ->distinct()
                ->where('users.co_usuario',$co_usuario)
                ->get();

        $contador = $fotopersona->count();
       

        if($contador>0){

            $foto = $fotopersona[0]->fimagen;

        }
        else{

             $foto=""; 

        }

    
        return($foto);
    }

    public function obtenernombrepersonaListarReservar()
    {
        $usuario_actual = \Auth::User();

        $co_usuario = $usuario_actual->co_usuario;

        $nombre = DB::table('users')
                ->select(
                    'users.nombreusuario AS personaaccesa'
                )
                ->distinct()
               ->where('users.co_usuario',$co_usuario)
                ->get();

        $nombrepersonaaccesa = $nombre[0]->personaaccesa;

        return($nombrepersonaaccesa);
    }




}
