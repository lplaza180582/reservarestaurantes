<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurante extends Model
{

	use SoftDeletes;

    protected $table = 'restaurante';
    protected $fillable = [
    //horasoli,codpersona,codconcepto
    'id',
    'nombre',
    'descripcion',
    'direccion',
    'ciudad',
    'foto',
    'urlfoto'
	];

    //protected $dates = ['deleted_at'];
    //
}
   