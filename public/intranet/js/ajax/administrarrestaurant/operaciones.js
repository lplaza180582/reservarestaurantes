
var busquedaGeneral = {
    dp_fedesde_admirestaurant: 'todos',
    dp_fehasta_admirestaurant: 'todos',
    tipo: "general",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_admirestaurant = 'todos';
    busquedaGeneral.dp_fehasta_admirestaurant = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function listaradmirestaurant() {

    //alert("pase");

    var route = "/listaradmirestaurant";

    var tipo="general"

    var recargartablatotal = jQuery('#listar_admirestaurant').dataTable({
        
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                data.dp_fedesde_admirestaurant = busquedaGeneral.dp_fedesde_admirestaurant;
                data.dp_fehasta_admirestaurant = busquedaGeneral.dp_fehasta_admirestaurant;
                data.tipo = busquedaGeneral.tipo;

            }

        },

        columns: [

            {
                data: 'id',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idrestaurant" value="' + data + '" class="flat-blue">' + data + '';
                }
            },
            {
                className: 'text-left',
                name: 'nombre',
                data: 'nombre'
                
            },
            {
                className: 'text-left',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'direccion',
                data: 'direccion'
                
            },
            {
                className: 'text-center',
                name: 'ciudad',
                data: 'ciudad'
                
            }
        
        ],

        language: lenguaje

    });

    

}

$("#btn_admirestaurant_crear").on('click', function() {

    ocultarmensajesmodalescrearadmirestaurant();
    ocultarmensajesmodalesvalidacioncradmirestaurant();
    ocultarmensajesindexadmirestaurant();

    $(".modal-body input").val("");

    //var blanco = "";
    //$("#textarea_descripcion_crconsultas").val(blanco);

    $("#btnmodalaceptar_admirestaurant").removeAttr('disabled');
    $("#btnmodalcancelar_admirestaurant").removeAttr('disabled');


    $('#admirestaurant_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});

$("#btnmodalaceptar_cradmirestaurant").on('click', function() {

    ocultarmensajesmodalesvalidacioncradmirestaurant();

    $("#btnmodalaceptar_cradmirestaurant").attr("disabled", "disabled");

    var nombre = $("#txt_nombre_cradmirestaurant").val();

    var descripcion = $("#txt_descripcion_cradmirestaurant").val();

    var direccion = $("#txt_direccion_cradmirestaurant").val();
    
    var ciudad = $("#txt_ciudad_cradmirestaurant").val();

    var foto= $('#soporte_cradmirestaurant').attr('src');
    
    
    if (nombre == "") {

        $("#msj-nombre-cradmirestaurant").fadeIn();
        $('#admirestaurant_crvalidacion').modal('show');
        $("#btnmodalaceptar_cradmirestaurant").removeAttr('disabled');
        return false;

    } 
    

    if (descripcion == "") {

        $("#msj-descripcion-cradmirestaurant").fadeIn();
        $('#admirestaurant_crvalidacion').modal('show');
        $("#btnmodalaceptar_cradmirestaurant").removeAttr('disabled');
        return false;

    } 

    if (direccion == "") {

        $("#msj-direccion-cradmirestaurant").fadeIn();
        $('#admirestaurant_crvalidacion').modal('show');
        $("#btnmodalaceptar_cradmirestaurant").removeAttr('disabled');
        return false;

    } 

    if (ciudad == "") {

        $("#msj-ciudad-cradmirestaurant").fadeIn();
        $('#admirestaurant_crvalidacion').modal('show');
        $("#btnmodalaceptar_cradmirestaurant").removeAttr('disabled');
        return false;

    } 

    var route = "/ingresarrestaurant";
    var token = $("#token").val();

    $.ajax({
        url: route,
        headers: {
            'X-CSRF-TOKEN': token
        },
        type: 'POST',
        dataType: 'json',

        data: {
            nombre: nombre,
            descripcion: descripcion,
            direccion: direccion,
            ciudad: ciudad,
            foto: foto
        },
        success: function(msj) {

            if (msj.mensaje == 'creado') {

                //setTimeout(function() {
                
                $("#msj-success-cradmirestaurant").fadeIn();
                //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                setTimeout(function() {
                    $('#admirestaurant_crear').modal('hide')
                }, 1500);

                reestablecerBusquedaGeneral();
                jQuery('#listar_admirestaurant').DataTable().search('');
                jQuery('#listar_admirestaurant').DataTable().ajax.reload();

               
                //}, 1500);
            } else {
                $('#msj-error-admirestaurant').text(msj.error);
                $("#msj-error-admirestaurant").removeClass('hidden');
                $("#btnmodalaceptar_admirestaurant").removeAttr('disabled');
            }

        },
        error: function(msj) {
        }
    });

});

$('#files_foto_cradmirestaurant').on('change', function(ev) {

    ocultarmensajesmodalesvalidacioncradmirestaurant();

    var f = ev.target.files[0];
    var fr = new FileReader();

    var fileLength = this.files.length;

    var fileSize = $('#files_foto_cradmirestaurant')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);

    var archivo = $("#files_foto_cradmirestaurant").val();
    var extension = archivo.substring(archivo.lastIndexOf("."));

    //alert(siezekiloByte);

    //MAXIMO 4MB  DE FOTO

    //if(siezekiloByte>4000){
    if(siezekiloByte>500){  

        //$("#files_foto_generarticket").height(200);
        //$("#files_foto_generarticket").width(200);
        $("#msj-tamañoimagennopermitida-cradmirestaurant").fadeIn();
        var textocargarimagen= "Cargue Imagen Restaurante";
        $('#seleccionartextoarchivo_cradmirestaurant').empty();
        $('#seleccionartextoarchivo_cradmirestaurant').text(textocargarimagen);
        $("#files_foto_cradmirestaurant").val("");
        $("#admirestaurant_crvalidacion").modal("show");
        return false;
    }

    for (var i=0; i < fileLength ; i++) {
      
      var archivo = ev.target.files[i].type;

      var extension = archivo.substring(archivo.lastIndexOf("/"));

      if((extension!="/jpeg")&&(extension!="/png")){

        $("#msj-formatoimagennopermitido-cradmirestaurant").fadeIn();
        var textocargarimagen= "Cargue Imagen Restaurante";
        $('#seleccionartextoarchivo_cradmirestaurant').empty();
        $('#seleccionartextoarchivo_cradmirestaurant').text(textocargarimagen);
        $("#files_foto_cradmirestaurant").val("");
        $("#admirestaurant_crvalidacion").modal("show");
        return false;
      }

    }

    fr.onload = function(ev2) {
        //console.dir(ev2);
        $('#soporte_cradmirestaurant').attr('src', ev2.target.result);
    };
    
    fr.readAsDataURL(f);

     
});

$("#borrar_cradmirestaurant").on('click', function () {
    var blanco="";
    var textocargarimagen= "Cargue Imagen Restaurante";
    $('#soporte_cradmirestaurant').attr('src', '');
    $('#textoarchivo_cradmirestaurant').val(blanco);
    $('#seleccionartextoarchivo_cradmirestaurant').empty();
    $('#seleccionartextoarchivo_cradmirestaurant').text(textocargarimagen);

});

$("#btn_admirestaurant_editar").on('click', function() {

    ocultarmensajesmodaleseditaradmirestaurant();
    ocultarmensajesmodalesvalidacionedadmirestaurant();
    ocultarmensajesindexadmirestaurant();

    var valorregistroeditar= $('input:radio[name=idrestaurant]:checked').val();

    if (valorregistroeditar==undefined){

        $("#msj-noexistenregistros-admirestaurant").fadeIn();
        return false;
    }
    else{

        //var blanco = "";
        //$("#textarea_descripcion_crconsultas").val(blanco);

        $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
        $("#btnmodalcancelar_edadmirestaurant").removeAttr('disabled');


        $('#admirestaurant_editar').modal({
            backdrop: 'static',
            keyboard: false
        });


        $('#textoarchivo_edadmirestaurant').empty();

        $("#files_foto_edadmirestaurant").val("");

        $('#soporte_edadmirestaurant').attr('src', '');

        var route="/obtenerrestaurant/"+$('input:radio[name=idrestaurant]:checked').val() ;

        
        $.get(route , function(data) {
      
        }) 

        .fail(function() {
            alert( "error" );
        })

        .done(function(data) {

            //console.log(data);

            $("#txt_nombre_edadmirestaurant").val(data.nombre);
            $("#txt_descripcion_edadmirestaurant").val(data.descripcion);
            $("#txt_direccion_edadmirestaurant").val(data.direccion);
            $("#txt_ciudad_edadmirestaurant").val(data.ciudad);
            
            var imagen = data.foto;

            $('#soporte_edadmirestaurant').attr('src', "data:image/jpg;base64," + imagen);

            $("#btnmodalaceptar_edadmirestaurant").attr("data-editar",data.id);  
            
              
        });

    }

});


$("#btnmodalaceptar_edadmirestaurant").on('click',function(){

    ocultarmensajesmodalesvalidacionedadmirestaurant();

    $("#btnmodalaceptar_edadmirestaurant").attr("disabled", "disabled");
    var route="/actualizarrestaurant"

    var nombre = $("#txt_nombre_edadmirestaurant").val();

    var decripcion = $("#txt_descripcion_edadmirestaurant").val();

    var direccion = $("#txt_direccion_edadmirestaurant").val();

    var ciudad = $("#txt_ciudad_edadmirestaurant").val();

    var imagen= $('#soporte_edadmirestaurant').attr('src');

    if (nombre == "") {

            $("#msj-nombre-edadmirestaurant").fadeIn();
            $('#admirestaurant_edvalidacion').modal('show');
            $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
            return false;
    }

    if (decripcion == "") {

            $("#msj-descripcion-edadmirestaurant").fadeIn();
            $('#admirestaurant_edvalidacion').modal('show');
            $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
            return false;
    }

    if (direccion == "") {

            $("#msj-direccion-edadmirestaurant").fadeIn();
            $('#admirestaurant_edvalidacion').modal('show');
            $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
            return false;
    }

    if (ciudad == "") {

            $("#msj-ciudad-edadmirestaurant").fadeIn();
            $('#admirestaurant_edvalidacion').modal('show');
            $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
            return false;
    }

    $.post( route,{
     id: $("#btnmodalaceptar_edadmirestaurant").attr("data-editar"),
     nombre: $("#txt_nombre_edadmirestaurant").val(),
     descripcion: $("#txt_descripcion_edadmirestaurant").val(),
     direccion: $("#txt_direccion_edadmirestaurant").val(),
     ciudad: $("#txt_ciudad_edadmirestaurant").val(),
     imagen:imagen,
     "_token": $("#token").val()}, function() {
    //alert( "success" );
    })
    .fail(function(msj) {
   

    })

    .done(function(msj) {
        if (msj.mensaje == 'actualizado') {

            $("#msj-success-edadmirestaurant").fadeIn();

            //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
            setTimeout(function() {
                $('#admirestaurant_editar').modal('hide')
            }, 1500);

            reestablecerBusquedaGeneral();
            jQuery('#listar_admirestaurant').DataTable().search('');
            jQuery('#listar_admirestaurant').DataTable().ajax.reload();
        }
        else{

            $('#msj-error-edadmirestaurant').text(msj.error);
            $("#msj-error-edadmirestaurant").removeClass('hidden');
            $("#btnmodalaceptar_edadmirestaurant").removeAttr('disabled');
        }
    
    });
})

$('#files_foto_edadmirestaurant').on('change', function(ev) {

    ocultarmensajesmodalesvalidacionedadmirestaurant();

    var f = ev.target.files[0];
    var fr = new FileReader();

    var fileLength = this.files.length;

    var fileSize = $('#files_foto_edadmirestaurant')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);

    var archivo = $("#files_foto_edadmirestaurant").val();
    var extension = archivo.substring(archivo.lastIndexOf("."));

    //alert(siezekiloByte);

    //MAXIMO 4MB  DE FOTO

    //if(siezekiloByte>4000){
    if(siezekiloByte>500){  

        //$("#files_foto_generarticket").height(200);
        //$("#files_foto_generarticket").width(200);
        $("#msj-tamañoimagennopermitida-edadmirestaurant").fadeIn();
        var textocargarimagen= "Cargue Imagen Restaurante";
        $('#seleccionartextoarchivo_edadmirestaurant').empty();
        $('#seleccionartextoarchivo_edadmirestaurant').text(textocargarimagen);
        $("#files_foto_edadmirestaurant").val("");
        $("#admirestaurant_edvalidacion").modal("show");
        return false;
    }

    for (var i=0; i < fileLength ; i++) {
      
      var archivo = ev.target.files[i].type;

      var extension = archivo.substring(archivo.lastIndexOf("/"));

      if((extension!="/jpeg")&&(extension!="/png")){

        $("#msj-formatoimagennopermitido-edadmirestaurant").fadeIn();
        var textocargarimagen= "Cargue Imagen Restaurante";
        $('#seleccionartextoarchivo_edadmirestaurant').empty();
        $('#seleccionartextoarchivo_edadmirestaurant').text(textocargarimagen);
        $("#files_foto_edadmirestaurant").val("");
        $("#admirestaurant_edvalidacion").modal("show");
        return false;
      }

    }

    fr.onload = function(ev2) {
        //console.dir(ev2);
        $('#soporte_edadmirestaurant').attr('src', ev2.target.result);
    };
    
    fr.readAsDataURL(f);

     
});

$("#borrar_edadmirestaurant").on('click', function () {
    var blanco="";
    var textocargarimagen= "Cargue Imagen Restaurante";
    $('#soporte_edadmirestaurant').attr('src', '');
    $('#textoarchivo_edadmirestaurant').val(blanco);
    $('#seleccionartextoarchivo_edadmirestaurant').empty();
    $('#seleccionartextoarchivo_edadmirestaurant').text(textocargarimagen);

});

$("#btn_admirestaurant_eliminar").on('click', function() {

    ocultarmensajesmodaleseliminaradmirestaurant();
    ocultarmensajesindexadmirestaurant();

    var valorregistroeditar= $('input:radio[name=idrestaurant]:checked').val();

    if (valorregistroeditar==undefined){

        $("#msj-noexistenregistros-admirestaurant").fadeIn();
        return false;
    }
    else{

        //var blanco = "";
        //$("#textarea_descripcion_crconsultas").val(blanco);

        $("#btnmodalaceptar_eladmirestaurant").removeAttr('disabled');
        $("#btnmodalcancelar_eladmirestaurant").removeAttr('disabled');


        $('#admirestaurant_eliminar').modal({
            backdrop: 'static',
            keyboard: false
        });

        var route="/obtenerrestaurant/"+$('input:radio[name=idrestaurant]:checked').val() ;

        
        $.get(route , function(data) {
      
        }) 

        .fail(function() {
            alert( "error" );
        })

        .done(function(data) {

            $("#btnmodalaceptar_eladmirestaurant").attr("data-eliminar",data.id);  
             
        });
    }

});

$("#btnmodalaceptar_eladmirestaurant").on('click',function(){

    $("#btnmodalaceptar_eladmirestaurant").attr("disabled", "disabled");
   //var route="/actualizarmarca"
   
    var route = "/obtenerasociacionrestaurant/" + $('input:radio[name=idrestaurant]:checked').val()
    var token = $("#token").val();


    $.ajax({
    url: route,
    headers: {'X-CSRF-TOKEN': token},
    type: 'POST',
    dataType: 'json',
    
    data:{
    },
    success:function(msj){

            if (msj.restauranteencontrado=='SI') {
                //alert("pase por aqui"+msj.restauranteencontrado);
                $("#msj-asociadoeliminar-eladmirestaurant").fadeIn();
                //$("#msj-asociadoeliminar-eladmirestaurant").removeClass('hidden');
                $("#btnmodalaceptar_eladmirestaurant").removeAttr('disabled');
                return false;
            }

            else{

               var route = "/eliminarrestaurant/" + $('input:radio[name=idrestaurant]:checked').val();

                $.get(route, function(data){

                })

                .done(function(data) {

                    $("#msj-successeliminar-eladmirestaurant").fadeIn();
                 
                    setTimeout(function() {
                        $('#admirestaurant_eliminar').modal('hide')
                    }, 1500);

                    reestablecerBusquedaGeneral();
                    jQuery('#listar_admirestaurant').DataTable().search('');
                    jQuery('#listar_admirestaurant').DataTable().ajax.reload();


                })

                .fail(function() {

                    $('#msj-error-eladmirestaurant').text(msj.error);
                    $("#msj-error-eladmirestaurant").removeClass('hidden');
                    $("#btnmodalaceptar_eladmirestaurant").removeAttr('disabled');
                       
                });
            }
           
    },
    error:function(msj){

        //alert("pase por aqui");

        $("#msj-asociadoeliminar-eladmirestaurant").removeClass('hidden');
        $("#btnmodalaceptar_eladmirestaurant").removeAttr('disabled');
       

    }
  });

});

////////////////////////////FUNCIONES/////////////////////////////

function ocultarmensajesindexadmirestaurant(){

    $("#msj-fechamayor-admirestaurant").fadeOut();
    $("#msj-fechadesdemesactual-admirestaurant").fadeOut();
    $("#msj-fechahastamesactual-admirestaurant").fadeOut();
    $("#msj-fechahastadiaactual-admirestaurant").fadeOut();
    $("#msj-noexistenregistros-admirestaurant").fadeOut();
   
}

function ocultarmensajesmodalescrearadmirestaurant(){

    $("#msj-success-cradmirestaurant").fadeOut();
    $("#msj-error-cradmirestaurant").fadeOut();
 
}

function ocultarmensajesmodalesvalidacioncradmirestaurant(){

    $("#msj-nombre-cradmirestaurant").fadeOut();
    $("#msj-descripcion-cradmirestaurant").fadeOut();
    $("#msj-direccion-cradmirestaurant").fadeOut();
    $("#msj-ciudad-cradmirestaurant").fadeOut();
    $("#msj-tamañoimagennopermitida-cradmirestaurant").fadeOut();
    $("#msj-formatoimagennopermitido-cradmirestaurant").fadeOut();
   
}

function ocultarmensajesmodaleseditaradmirestaurant(){

    $("#msj-success-edadmirestaurant").fadeOut();
    $("#msj-error-edadmirestaurant").fadeOut();
 
}

function ocultarmensajesmodalesvalidacionedadmirestaurant(){

    $("#msj-nombre-edadmirestaurant").fadeOut();
    $("#msj-descripcion-edadmirestaurant").fadeOut();
    $("#msj-direccion-edadmirestaurant").fadeOut();
    $("#msj-ciudad-edadmirestaurant").fadeOut();
    $("#msj-tamañoimagennopermitida-edadmirestaurant").fadeOut();
    $("#msj-formatoimagennopermitido-edadmirestaurant").fadeOut();
   
}

function ocultarmensajesmodaleseliminaradmirestaurant(){

    $("#msj-successeliminar-eladmirestaurant").fadeOut();
    $("#msj-asociadoeliminar-eladmirestaurant").fadeOut();
    $("#msj-error-eladmirestaurant").fadeOut();
   
 
}


jQuery(function($) {

    setTimeout(function() {

     }, 500);

});


