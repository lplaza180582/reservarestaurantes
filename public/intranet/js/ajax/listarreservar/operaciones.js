
var busquedaGeneral = {
    dp_fedesde_listarreservar: 'todos',
    dp_fehasta_listarreservar: 'todos',
    tipo: "general",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_listarreservar = 'todos';
    busquedaGeneral.dp_fehasta_listarreservar = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function listarreservar() {

    //alert("pase");

    var route = "/listarreservas";

    var tipo="general"

    var recargartablatotal = jQuery('#listar_listarreservar').dataTable({
        
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                data.dp_fedesde_listarreservar = busquedaGeneral.dp_fedesde_listarreservar;
                data.dp_fehasta_listarreservar = busquedaGeneral.dp_fehasta_listarreservar;
                data.tipo = busquedaGeneral.tipo;

            }

        },

        columns: [

            {
                data: 'id',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idreserva" value="' + data + '" class="flat-blue">' + data + '';
                }
            },
            {
                className: 'text-left',
                name: 'nombre',
                data: 'nombre'
                
            },
            {
                className: 'text-left',
                name: 'restaurante',
                data: 'restaurante'
                
            },
            {
                className: 'text-center',
                name: 'fecha',
                data: 'fecha'
                
            }
            
        
        ],

        language: lenguaje

    });

    

}


$("#btn_listarreservar_buscar").on('click', function() {


    $("#msj-fechamayor-listarreservar").fadeOut();
    $("#msj-noexistenregistros-listarreservar").fadeOut();
    $("#msj-fechadesdemesactual-listarreservar").fadeOut();
    $("#msj-fechahastamesactual-listarreservar").fadeOut();
    $("#msj-fechahastadiaactual-listarreservar").fadeOut();
    
    var dp_fedesde_listarreservar = $("#dp_fedesde_listarreservar").val();
    var dp_fehasta_listarreservar = $("#dp_fehasta_listarreservar").val();


    var validarfechavalor = validar_fechas(dp_fedesde_listarreservar , dp_fehasta_listarreservar);

    var fullDate = new Date()

    var dia = ((fullDate.getDate().length) === 1) ? (fullDate.getDate()) : '0' + (fullDate.getDate());
    var mes = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);



    var fechaactual = fullDate.getFullYear() + "-" + mes + "-" + dia;

    var validarfechadesdelistarreservar = validar_fechasmesactual(dp_fedesde_listarreservar, fechaactual);

    var validarfechahastalistarreservar = validar_fechasmesactual(dp_fehasta_listarreservar, fechaactual);


    if (validarfechavalor == 0) {


        $("#msj-fechamayor-listarreservar").fadeIn();

        return false;

    }
    
    //SE INHABILITO LA VALIDACION QUE TIENE LA BUSQUEDA
    //SE AGREGO ESTE ELSE
    else{

        //alert("pase por aqui");

        busquedaGeneral.dp_fedesde_listarreservar = dp_fedesde_listarreservar;
        busquedaGeneral.dp_fehasta_listarreservar = dp_fehasta_listarreservar;
        busquedaGeneral.tipo = "especifico";
        busquedaGeneral.buscar = "";

        var tipo="especifico"
    
        jQuery('#listar_listarreservar').DataTable().search('');
        jQuery('#listar_listarreservar').DataTable().ajax.reload();

    } 
});


//////////////////////////////////////////////////////////////////////////////////////////

$("#btn_listarreservar_limpiar").on('click', function() {


    var limpiar = 'todos';

    $("#msj-fechamayor-listarreservar").fadeOut();
    $("#msj-noexistenregistros-listarreservar").fadeOut();
    $("#msj-fechadesdemesactual-listarreservar").fadeOut();
    $("#msj-fechahastamesactual-listarreservar").fadeOut();
    $("#msj-fechahastadiaactual-listarreservar").fadeOut();

    fechahoy=obtener_fechaactual();

    $("#dp_fedesde_listarreservar").val(fechahoy);
    $("#dp_fehasta_listarreservar").val(fechahoy);

    reestablecerBusquedaGeneral();

    var tipo="general"

    jQuery('#listar_listarreservar').DataTable().search('');
    jQuery('#listar_listarreservar').DataTable().ajax.reload();

});


jQuery(function($) {

    setTimeout(function() {

     }, 500);

});


