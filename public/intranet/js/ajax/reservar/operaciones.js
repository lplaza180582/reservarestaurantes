
var busquedaGeneral = {
    dp_fedesde_reservar: 'todos',
    dp_fehasta_reservar: 'todos',
    tipo: "general",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_reservar = 'todos';
    busquedaGeneral.dp_fehasta_reservar = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});

function filtrarDatarestaurant(data, term) {
    listarestaurant = [];
    var contiene;
    if ($(data).filter(function(index) {
            contiene = this.text.toLowerCase().indexOf(term.toLowerCase()) != -1;
            if (contiene) {
                listarestaurant.push(data[index]);
            }
            return contiene;
        }).length > 0) {
        return listarestaurant;
    } else {
        return listarestaurant;
    }
}

function select2resta(idrestaurant,datos,select2restaurant,mensajeNoEncontradorestaurant) {

    //alert("paso"+idconceptosincdia);
    //select2producto= 1;

    //select2conceptosincdia=2;

    jQuery(idrestaurant).select2({

        closeOnSelect: false,
        maximumSelectionSize: 1,
        multiple: true,
        initSelection: function(element, callback) {

            if (select2restaurant!= null) {
                 
                $.each(datos, function(key, registro) {
                    
                    if (registro.id == select2restaurant) {
                        callback(datos[key]);
                        return false;
                    }
                });
            } else {
                //alert("pase por aqui son iguales");
                //callback(datos[0]);
            }
        },

        formatSelectionTooBig: function(limit) {
            return 'S\u00F3lo puede seleccionar un Restaurante';
        },
        createSearchChoice: function(term, data) {
            filtrarDatarestaurant(data, term);
        },
        formatNoMatches: function() {
            return mensajeNoEncontradorestaurant;
        },
        query: function(options) {

            if (options.term.length >= 2) {

                var criterio = "";

                criterio = options.term;

                var ruta = "/recargarrestaurant/" + criterio;

                listarrestaurant(idrestaurant, ruta, null, options.term);

                setTimeout(function() {
                    options.callback({
                        results: listarestaurant
                    });
                }, 1000);
            } else {
                if (options.term != "") {
                    listarestaurant = filtrarDatarestaurant(listarestaurant, options.term);
                    options.callback({
                        results: listarestaurant
                    });
                } else {
                    options.callback({
                        results: listarestaurant
                    });
                }

            }
        },
        placeholder: "Por Favor Seleccione el Restaurante",
        data: datos
    });

    select2Defecto(idrestaurant);

}

function select2Defecto(identificador) {
    jQuery(identificador).select2('val', []);
}

var listarestaurant = [];

function listarrestaurant(idrestaurant, ruta, select2restaurant = null, buscar = null) {
    //var route = buscar != null ? ruta + buscar : ruta;
    var mensajeNoEncontradorestaurant = 'No se encontr\u00F3 Restaurantes'
    var mensajeFalloCarga = 'Fallo el cargado de Restaurantes';
    listarestaurant = [];
    $.get(ruta, function(data) {
            listarestaurant = [];
            $.each(data, function(key, registro) {
                //alert(registro.codconcepto);
                listarestaurant.push({
                    id: registro.id,
                    text: registro.nombre
                });
            });

            if (buscar == null) {

                //console.log(listaconceptosincdia);
                //console.log(idconceptosincdia);

                select2resta(idrestaurant,listarestaurant,select2restaurant,mensajeNoEncontradorestaurant);
            }
        })
        .fail(function() {
            select2resta(idrestaurant,listarestaurant,select2restaurant,mensajeFalloCarga);
        });
}


function listarreservar() {

    //alert("pase");

    var route = "/listarreservar";

    var tipo="general"

    var recargartablatotal = jQuery('#listar_reservar').dataTable({
        
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                data.dp_fedesde_reservar = busquedaGeneral.dp_fedesde_reservar;
                data.dp_fehasta_reservar = busquedaGeneral.dp_fehasta_reservar;
                data.tipo = busquedaGeneral.tipo;

            }

        },

        columns: [

            {
                data: 'id',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idreserva" value="' + data + '" class="flat-blue">' + data + '';
                }
            },
            {
                className: 'text-left',
                name: 'nombre',
                data: 'nombre'
                
            },
            {
                className: 'text-left',
                name: 'restaurante',
                data: 'restaurante'
                
            },
            {
                className: 'text-center',
                name: 'fecha',
                data: 'fecha'
                
            }
            
        
        ],

        language: lenguaje

    });

    

}

$("#btn_reservar_crear").on('click', function() {

    ocultarmensajesmodalescrearreservar();
    ocultarmensajesmodalesvalidacioncrreservar();

    $(".modal-body input").val("");

    fechahoy=obtener_fechaactual();

    $("#dp_fe_crreservar").val(fechahoy);

    //var blanco = "";
    //$("#textarea_descripcion_crconsultas").val(blanco);

    $("#btnmodalaceptar_reservar").removeAttr('disabled');
    $("#btnmodalcancelar_reservar").removeAttr('disabled');

    var idrestaurante = "#sp_restaurante_crreservar";
    var route = "/cargarrestaurante";
    //alert(route);
    listarrestaurant(idrestaurante,route)


    $('#reservar_crear').modal({
        backdrop: 'static',
        keyboard: false
    });

});

$("#btnmodalaceptar_crreservar").on('click', function() {

    ocultarmensajesmodalesvalidacioncrreservar();

    $("#btnmodalaceptar_crreservar").attr("disabled", "disabled");

    var nombrereserva = $("#txt_nombre_crreservar").val();

    var fecha = $("#dp_fe_crreservar").val();

    var restaurante= $("#sp_restaurante_crreservar").val();
    
    
    if (nombrereserva == "") {

        $("#msj-nombrereserva-crreservar").fadeIn();
        $('#reservar_crvalidacion').modal('show');
        $("#btnmodalaceptar_crreservar").removeAttr('disabled');
        return false;

    } 
    

    if (fecha == "") {

        $("#msj-fecha-crreservar").fadeIn();
        $('#reservar_crvalidacion').modal('show');
        $("#btnmodalaceptar_crreservar").removeAttr('disabled');
        return false;

    } 

    if (restaurante == "") {

        $("#msj-restaurante-crreservar").fadeIn();
        $('#reservar_crvalidacion').modal('show');
        $("#btnmodalaceptar_crreservar").removeAttr('disabled');
        return false;

    } 

    var token = $("#token").val();

    var route = "/buscarcantidadreservas/"+ restaurante +"/"+fecha;

    
    $.get(route, function (data) {
    })

    .done(function (data) {

        //alert(data.cantidadreservas);

        if (data.cantidadreservas == 'CANTIDAD DE RESERVAS SUPERADA') {

          $("#msj-maxcantidadreserva-crreservar").fadeIn();
          $('#reservar_crvalidacion').modal('show');
          $("#btnmodalaceptar_crreservar").removeAttr('disabled');
          $("#btnmodalcancelar_crreservar").removeAttr('disabled');
          return false;
        }
        else {

            var route = "/ingresarreserva";
            var token = $("#token").val();

            $.ajax({
                url: route,
                headers: {
                    'X-CSRF-TOKEN': token
                },
                type: 'POST',
                dataType: 'json',

                data: {
                    nombrereserva: nombrereserva,
                    fecha: fecha,
                    restaurante: restaurante
                },
                success: function(msj) {

                    if (msj.mensaje == 'creado') {

                        //setTimeout(function() {
                        
                        $("#msj-success-crreservar").fadeIn();
                        //LUEGO DE MOSTRAR EL MENSAJE CIERRA EL MODAL DE MANERA AUTOMATICA
                        setTimeout(function() {
                            $('#reservar_crear').modal('hide')
                        }, 1500);

                        reestablecerBusquedaGeneral();
                        jQuery('#listar_reservar').DataTable().search('');
                        jQuery('#listar_reservar').DataTable().ajax.reload();

                       
                        //}, 1500);
                    } else {
                        $('#msj-error-reservar').text(msj.error);
                        $("#msj-error-reservar").removeClass('hidden');
                        $("#btnmodalaceptar_reservar").removeAttr('disabled');
                    }

                },
                error: function(msj) {
                }
            });

        }

    })
});


////////////////////////////FUNCIONES/////////////////////////////

function ocultarmensajesmodalescrearreservar(){

    $("#msj-success-crreservar").fadeOut();
    $("#msj-error-crreservar").fadeOut();
}

function ocultarmensajesmodalesvalidacioncrreservar(){

    $("#msj-restaurante-crreservar").fadeOut();
    $("#msj-fecha-crreservar").fadeOut();
    $("#msj-nombre-crreservar").fadeOut();  
}

function recargarrestaurant(criterio){


    var route = "/recargarrestaurant/" + criterio;

    $.get(route, function(data) {

    })

    .done(function(data) {

        //cambio
        registros_conceptosincdia = [];

        $.each(data, function(key, registro) {

            if (data == "") {

                registros[0] = "";

            } else {

                registros_conceptosincdia.push({
                        id: data[key].id,
                        text: data[key].nombre
                });

            }

        });

    })

    .fail(function() {
        alert("error");

    });

}




jQuery(function($) {

    setTimeout(function() {

     }, 500);

});


