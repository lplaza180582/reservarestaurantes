

 $("#btnmodalcerrar_login").click(function(){

       //alert("pase por aqui");

      var blanco = "";
      $('.errorcaptcha').text(blanco);
      $('.errorcaptcha').fadeOut();

   
       var usuario = $('#tx_usuario_login').val();
       var clave = $('#tx_clave_login').val();
       
       var latitud= $('#txtoculto_latitud_login').val();

       var longitud= $('#txtoculto_longitud_login').val();

       var captcha=$('#captcha').val();

       //alert(captcha);


      if (clave == '') {

           $("#msj-campoclave-login").fadeIn();
                    $("#login_validacion").modal("show");

         return false;
      }


      if (usuario == '') {

                    $("#msj-campousuario-login").fadeIn();
                    $("#login_validacion").modal("show");
        return false;

       }else{

        
        var token = $("#token").val();

        var route = "/buscar_usuario/" + usuario;



        //var route = "/estatus_usuario/" + usuario;


         //alert(route);

    
        $.get(route, function (data) {
            })
                .done(function (data) {

                
                    if (data=='NO EXISTE USUARIO') {
                        var estatus_usuario = 'NO EXISTE USUARIO';
                         
                    }else{

                        if (data=='CAMPO CLAVE VACIO') {
                        
                           var estatus_usuario = 'CAMPO CLAVE VACIO';

                        }
                        else{

                            var estatus_usuario = data[0].activo;
                        }
 
                    }



                   //alert(estatus_usuario);

                    if (estatus_usuario == 'S') {
                            
                          var route = "/validarcaptcha";

                         
                          var token = $("#token").val();


                          $.ajax({
                              url: route,
                              headers: {'X-CSRF-TOKEN': token},
                              type: 'POST',
                              dataType: 'json',

                              data: {
                                  captcha:captcha
                              },
                              success: function (data) {

                                  if(data.msj=="captchacorrecto"){

                                    var route = "/iniciar_sesion";
                                    var token = $("#token").val();

                                    //alert(route);

                                    $.ajax({
                                        url: route,
                                        headers: {'X-CSRF-TOKEN': token},
                                        type: 'POST',
                                        dataType: 'json',

                                        data: {
                                            usuario: usuario,
                                            clave: clave,
                                            latitud: latitud,
                                            longitud: longitud
                                        },
                                        success: function (msj) {

                                            if (msj == '1') {

                                               //alert(msj);

                                                var url = "/admin";
                                               
                                                $(location).attr('href',url);

                                               
                                            }
                                            else if (msj == '-1') {
                                                

                                               $("#msj-usuario-login").fadeIn();
                                                $("#login_validacion").modal("show");
                                            
                                            }else{

                                              $("#msj-clave-login").fadeIn();
                                                $("#login_validacion").modal("show");
                                            }

                                        },

                                        error: function (msj) {

                                        }
                                    });
                                  }
                                  if(data.msj=="captchaenblanco"){

                                    //alert("pase por aqui");

                                    var mensaje= "Debe Introducir El Codigo de la Imagen";
                                    $('.errorcaptcha').text(mensaje);
                                    $('.errorcaptcha').fadeIn();

                                  }

                                  if(data.msj=="captchaincorrecto"){

                                    //alert("pase por aqui");

                                    var mensaje= " El Codigo es Incorrecto";
                                    $('.errorcaptcha').text(mensaje);
                                    $('.errorcaptcha').fadeIn();

                                    var blanco="";
                                    $("#captcha").val(blanco);
                                    refrescarcaptcha();

                                  }

                              },
                              
                              error: function (msj) {

                              }
                          });
                    }else if (estatus_usuario == 'N'){

                         $("#msj-usuarioinvalido-login").fadeIn();
                         $("#login_validacion").modal("show");
                         return false;
                    }else{ 

                        if (estatus_usuario == 'NO EXISTE USUARIO'){

                          $("#msj-usuario-login").fadeIn();
                          $("#login_validacion").modal("show");
                          return false;

                        }

                        if (estatus_usuario == 'CAMPO CLAVE VACIO'){

                          $("#msj-clavevaciapersona-login").fadeIn();
                          $("#login_validacion").modal("show");
                          return false;

                        }
                    }

                })

                .fail(function () {
                    alert("error");

                });



       }



    

});


$("#btn_cerrarsesion_login").on('click', function () {


        var route = "/cerrar_sesion";
 
        var token = $("#token").val();

        var blanco = "";
        var captcha=$('#captcha').val();


        $.ajax({
            url: route,
            headers: {'X-CSRF-TOKEN': token},
            type: 'GET',
            dataType: 'json',

         
            success: function (msj) {

              
              
                if (msj == '1') {
                   
                 //   var url = "/admin";
                   
                  // $(location).attr('href',url);
                  alert('salio');

                }
               else{
                alert('o salio')
                //  $("#msj-clave-login").fadeIn();
                 //   $("#login_validacion").modal("show");
                }

            },
            error: function (msj) {

                alert('error');
            }
        });
   

});


$("#btnmodalaceptar_login").on('click', function () {


    $("#msj-usuario-login").fadeOut();
    $("#msj-clave-login").fadeOut();
    $("#msj-campousuario-login").fadeOut();
    $("#msj-campoclave-login").fadeOut();
    $("#msj-clavevaciapersonal-login").fadeOut();
    $("#msj-usuarioinvalido-login").fadeOut();

});


$("#tx_clave_login").keypress(function(e) {

     if (e.which == 13) {
         var login = $("#tx_clave_login").val();
         var clave = $("#tx_usuario_login").val();

         if ((login != "") && (clave != "")) {
             $("#btnmodalcerrar_login").trigger("click");
         }
     }

 });


 $("#tx_usuario_login").keypress(function(e) {

     if (e.which == 13) {
         var login = $("#tx_clave_login").val();
         var clave = $("#tx_usuario_login").val();

         if ((login != "") && (clave != "")) {
             $("#btnmodalcerrar_login").trigger("click");
         }
     }

 });

$("#refrescar_captcha").on('click', function () {

    //alert("pase por aqui");

    var blanco="";
    $("#captcha").val(blanco);

    $.ajax({
        url:'refrescarcaptcha',
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function (data) {

           $(".captcha span").html(data.captcha);                       
                                
        },
        error: function (data) {

            alert('error');
        }
    });

 });

function refrescarcaptcha(){

    //alert("pase por aqui");


    var token = $("#token").val();

    $.ajax({
        url:'refrescarcaptcha',
        headers: {'X-CSRF-TOKEN': token},
        type: 'GET',
        dataType: 'json',
        success: function (data) {

           //alert("si recargo");

           $(".captcha span").html(data.captcha);                       
                                
        },
        error: function (data) {

            alert('error');
        }
    });

}





 