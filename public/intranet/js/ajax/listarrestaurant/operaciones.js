
var busquedaGeneral = {
    dp_fedesde_listarrestaurant: 'todos',
    dp_fehasta_listarrestaurant: 'todos',
    tipo: "general",
    buscar: ""
};

function reestablecerBusquedaGeneral() {
    busquedaGeneral.dp_fedesde_listarrestaurant = 'todos';
    busquedaGeneral.dp_fehasta_listarrestaurant = 'todos';
    busquedaGeneral.tipo = 'general';
    busquedaGeneral.buscar = '';
}

jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': jQuery("#token").val()
    }
});


function listarrestaurante() {

    //alert("pase");

    var route = "/listarrestaurant";

    var tipo="general"

    var recargartablatotal = jQuery('#listar_listarrestaurant').dataTable({
        
       
        "processing": true,
        "serverSide": true,
        "ajax": {
            method: 'post',
            url: route,
            "data": function(data) {

                //alert("paso");

                data.dp_fedesde_listarrestaurant = busquedaGeneral.dp_fedesde_listarrestaurant;
                data.dp_fehasta_listarrestaurant = busquedaGeneral.dp_fehasta_listarrestaurant;
                data.tipo = busquedaGeneral.tipo;

            }

        },

        columns: [

            {
                data: 'id',
                render: function(data, type, row) {

                    return '<input type="radio"  name="idrestaurant" value="' + data + '" class="flat-blue">' + data + '';
                }
            },
            {
                className: 'text-left',
                name: 'nombre',
                data: 'nombre'
                
            },
            {
                className: 'text-left',
                name: 'descripcion',
                data: 'descripcion'
                
            },
            {
                className: 'text-center',
                name: 'direccion',
                data: 'direccion'
                
            },
            {
                className: 'text-center',
                name: 'ciudad',
                data: 'ciudad'
                
            },

            {
                className: 'text-center',
                name: 'urlfoto',
                data: 'urlfoto'
                
            }
        
        ],

        language: lenguaje

    });

    

}


jQuery(function($) {

    setTimeout(function() {

     }, 500);

});


