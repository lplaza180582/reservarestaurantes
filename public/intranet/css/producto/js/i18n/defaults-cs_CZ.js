/*!
 * bootstrap-selectpro v1.12.4 (http://silviomoreto.github.io/bootstrap-selectpro)
 *
 * Copyright 2013-2017 bootstrap-selectpro
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-selectpro/blob/master/LICENSE)
 */

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module unless amdModuleId is set
    define(["jquery"], function (a0) {
      return (factory(a0));
    });
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory(require("jquery"));
  } else {
    factory(root["jQuery"]);
  }
}(this, function (jQuery) {

(function ($) {
  $.fn.selectpickerpro.defaults = {
    noneSelectedText: 'Nic není vybráno',
    noneResultsText: 'Žádné výsledky {0}',
    countSelectedText: 'Označeno {0} z {1}',
    maxOptionsText: ['Limit překročen ({n} {var} max)', 'Limit skupiny překročen ({n} {var} max)', ['položek', 'položka']],
    multipleSeparator: ', ',
    selectAllText: 'Vybrat Vše',
    deselectAllText: 'Odznačit Vše'
  };
})(jQuery);


}));
