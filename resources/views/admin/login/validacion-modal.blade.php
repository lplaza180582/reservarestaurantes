{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="login_validacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Inicio Sesión</h4>
            </div>
            <div class="modal-body">

                    <div id="msj-campousuario-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>El campo Usuario no puede estar vacío</strong>
                    </div>

                   <div id="msj-campoclave-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>El campo clave del Usuario no puede estar vacio</strong>
                    </div>


                    <div id="msj-usuario-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>El Usuario no existe</strong>
                    </div>


                    <div id="msj-usuarioinvalido-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>El Usuario se encuentra Inactivo</strong>
                    </div>

                    <div id="msj-clave-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>La clave del Usuario es incorrecta</strong>
                    </div>

                    <div id="msj-clavevaciapersona-login" class="alert alert-warning alert-dismissible" role="" style="display:none">
                    <strong>En estos momentos nuestra plataforma no se encuentra disponible</strong>
                    </div>

                    




            </div>
            <div class="modal-footer">
                <button id="btnmodalaceptar_login"  type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}


