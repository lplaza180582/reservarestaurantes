@extends('admin.layout.admin')
@section('content')



    <!-- datatables -->

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- datatables -->

    <!-- Datepicker -->
    <!-- Jquery -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.css">

    <!-- Datepicker Files -->
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">

    <script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
    <!-- Languaje -->
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>

    <!-- Datepicker Files -->

    @include('alerts.errors')


    @if(count($errors) > 0)
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{!!$error!!}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Listar Restaurantes
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Listar Restaurantes</li>
        </ol>

    </section>


    <section class="content">
       
        <div class="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        </div>   

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Listado Restaurantes</h3>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table id="listar_listarrestaurant" class="table table-condensed table-hover" style="width:100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NOMBRE</th>
                            <th>DESCRIPCION</th>
                            <th>DIRECCION</th>
                            <th>CIUDAD</th>
                            <th>FOTO</th>
                        </tr>
                        
                        </thead>

                  
                    </table>
                </div>
            </div>
        </div>   
    </section>

    <tfoot>




@endsection
@section('scripts')

    <!-- datepicker -->
    {!!Html::script('intranet/css/datepicker/js/bootstrap-datepicker.js')!!}
    <!-- datepicker -->

    <!-- datatables -->
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <!-- datatables -->


     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js"></script>

    {!!Html::script('intranet/js/ajax/operacionescomunes/operaciones.js')!!}
    {!!Html::script('intranet/js/ajax/listarrestaurant/operaciones.js')!!}


    <script type="text/javascript">

        var lenguaje = {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },

            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },

        }


        jQuery.noConflict();

        jQuery(document).ready(function () {

        listarrestaurante();

        
        }); 


       // $('input:radio[id=rb_pago1_crventa]')[0].checked = true;

      //  $('input:radio[id=rb_pago2_crventa]')[0].checked = false;

 
    </script>



@endsection