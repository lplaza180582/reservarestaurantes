{!!Form::open()!!}
<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
<div class="modal fade" id="admirestaurant_crvalidacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:500px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Validacion Restaurante</h4>
            </div>
            <div class="modal-body">

                <div id="msj-nombre-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Ingresar El Nombre</strong>
                </div>

                <div id="msj-descripcion-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Ingresar La Descripcion</strong>
                </div>

                <div id="msj-direccion-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Ingresar La Direccion</strong>
                </div>

                <div id="msj-ciudad-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none">
                <strong>Debe Ingresar La Ciudad</strong>
                </div>

                <div id="msj-tamañoimagennopermitida-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none" align="justify">
                <!--<strong>Tamaño Imagen Supera 4MB(Max Permitido) </strong>-->
                <strong>Tamaño Imagen Supera 500KB(Max Permitido) </strong>
                </div>

                <div id="msj-formatoimagennopermitido-cradmirestaurant" class="alert alert-warning alert-dismissible" role="" style="display:none" align="justify">
                <strong>Formato Imagen No Permitido </strong>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><span
                            class="glyphicon glyphicon-ok"></span> Aceptar
                </button>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 