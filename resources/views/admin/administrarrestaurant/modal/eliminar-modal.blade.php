
{!!Form::open()!!}
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">


                                 
                                    <div class="modal fade" id="admirestaurant_eliminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Elimine Registro Seleccionado</h4>
                                                </div>

                                                <div id="msj-successeliminar-eladmirestaurant" class="msj-successeliminar alert alert-success alert-dismissible" style="">
                                                    <strong> Restaurante Eliminado Correctamente.</strong>
                                                </div>

                                                <div id="msj-error-eladmirestaurant" class="alert alert-warning alert-dismissible" > </div>

                                                <div id="msj-asociadoeliminar-eladmirestaurant" class="alert alert-warning alert-dismissible" role="">
                                                    <strong>El dato que desea eliminar esta siendo utilizado en otros registros, por lo tanto no puede ser eliminado</strong>
                                                </div>

                                                <div class="modal-body">
                                                    <h4><p align="center" >Desea Eliminar el Registro?</p></h4>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="btnmodalaceptar_eladmirestaurant" data-eliminar="" class="btn btn-primary" > <span class="glyphicon glyphicon-ok"></span> Aceptar</button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> Cancelar</button> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </form> 
{!!Form::close()!!}