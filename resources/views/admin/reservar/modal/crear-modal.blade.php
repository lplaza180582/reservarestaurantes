{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="reservar_crear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

   

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"Reservar>&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Inserte los datos de la Reserva</h4>
            </div>
             <div id="msj-success-crreservar" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Reserva Agregada Correctamente.</strong>
        </div>
        <div id="msj-error-crreservar" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                <div class="btn-group" >                         
                     <label for="">Restaurante</label> </b>
                     <div class="form-group">
                        <input id="sp_restaurante_crreservar" multiple="multiple"   class="form-control"   style="width:270px" >
                     </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="btn-group">
                    <label>Fecha</label> </b>
                    <div class="form-group">
                        <div class="input-group input-append date" ">
                            <input type="text" class="form-control " id="dp_fe_crreservar"> 
             
                        </div>
                    </div>
                </div>&nbsp;&nbsp;&nbsp;&nbsp;

                <div class="btn-group">
                      <label>Nombre Reserva</label>
                       <input type="text" class="form-control" id="txt_nombre_crreservar" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" placeholder="Introduzca Nombre" size="50"> 
                </div>

            </div>

            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_crreservar" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_crreservar" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>