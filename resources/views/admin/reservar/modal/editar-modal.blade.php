{!!Form::open()!!}
<input type="hidden" name="_token" value="{{csrf_token() }}" id="token">
<div class="modal fade" id="admirestaurant_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

   

    <div class="modal-dialog" role="document" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"AdministrarRestaurante>&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Modifique los datos del registro seleccionado</h4>
            </div>
             <div id="msj-success-edadmirestaurant" class=" msj-success alert alert-success alert-dismissible" role="" >
        <strong> Restaurante Actualizado Correctamente.</strong>
        </div>
        <div id="msj-error-edadmirestaurant" class="alert alert-warning alert-dismissible" role="" >
        <strong></strong>
        </div>

          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> <label style="display: none" id="procesainfo">Procesando Informacion Por Favor Espere</label></b>

            <div class="btn-group">
                <button style="display: none" type="button" class="btn btn-success btn-lg " id="load"
                      data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i>">
                </button>
            </div>
            
        <div class="modal-body">

                <div class="form-group">
                      <label>Nombre</label>
                       <input type="text" class="form-control" id="txt_nombre_edadmirestaurant" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" placeholder=""> 
                </div>

                <div class="form-group">
                      <label>Descripcion</label>
                       <input type="text" class="form-control" id="txt_descripcion_edadmirestaurant" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" placeholder=""> 
                </div>

                <div class="form-group">
                      <label>Direccion</label>
                       <input type="text" class="form-control" id="txt_direccion_edadmirestaurant" onkeypress="return validarteclaenterconvertirmayusminusletrasnumeros(event,this)" placeholder=""> 
                </div>

                <div class="form-group">
                      <label>Ciudad</label>
                       <input type="text" class="form-control" id="txt_ciudad_edadmirestaurant" onkeypress="return validarteclaenterconvertirmayusminussololetras(event,this)" placeholder=""> 
                </div>

                <div class="form-group">
                      <label>Imagen</label>
                </div>

                <div id="divcargarimagen_edadmirestaurant"  class="form" >
                        <div class="">

                            <span style="display:inline;width:100px" id="seleccionartextoarchivo_edadmirestaurant" class="form-control" disabled="" >Cargue Imagen Restaurante</span> 
                            
                            <span class="btn btn-success" onclick="$(this).parent().find('input[type=file]').click();">Examinar</span> 

                            <input id="files_foto_edadmirestaurant" name="uploaded_file" onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());" style="display: none;" type="file" multiple="" >


                            <br>

                            <img id="soporte_edadmirestaurant" class="img-circle" width="65" height="65"/>
                             
                            
                            <br>

                            
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type = "button" id = "borrar_edadmirestaurant" class = "btn btn-danger btn-danger"> <i class = "fa fa-close"> </i> </button>

                            <input id="textoarchivo_edadmirestaurant" type="hidden" />
                        
                            <p class="help-block"  >Tama&ntildeo Imagen: Max. 500 KB; Formato: "JPG" "PNG"</p>
                           
                        </div>
                        
                    </div> 

            </div>
            <div class="modal-footer">

                 <div style="text-align: right" class="box-footer">
                    <button type="button" id="btnmodalaceptar_edadmirestaurant" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Aceptar
                    </button>
                    <button type="button" id="btnmodalcancelar_edadmirestaurant" class="btn btn-danger" data-dismiss="modal"><span
                            class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
{!!Form::close()!!}

 <script type="text/javascript">

    var BACKSPACE_NAV_DISABLED = true; 

    function fnPreventBackspace(event){if (BACKSPACE_NAV_DISABLED && event.keyCode == 8) {return false;}} 
    //function fnPreventBackspacePropagation(event){if(BACKSPACE_NAV_DISABLED && event.keyCode == 8){event.stopPropagation();}return true;} 

    $(document).ready(function(){ 
     if(BACKSPACE_NAV_DISABLED){ 
      //for IE use keydown, for Mozilla keypress 
      //as described in scr: http://www.codeproject.com/KB/scripting/PreventDropdownBackSpace.aspx 
      $(document).keypress(fnPreventBackspace); 
      //$(document).keydown(fnPreventBackspace); 

      //Allow Backspace is the following controls 
      
     } 
    }); 

</script>